﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HeadOrTail
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        private int tryCountSort = 0;
        private int headsCountSort = 0;
        private int tailsCountSort = 0;
        private String actualState = "";

        private static System.Timers.Timer aTimer;


        public MainWindow()
        {
            InitializeComponent();

            SetTimer();
            fullTail.Visibility = Visibility.Collapsed;
            midTail.Visibility = Visibility.Collapsed;
            fullHead.Visibility = Visibility.Collapsed;
            midHead.Visibility = Visibility.Collapsed;
            midCoin.Visibility = Visibility.Collapsed;
        }

        private static void SetTimer()
        {
            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(2000);
            aTimer.AutoReset = true;
            aTimer.Enabled = true;
        }

        public void Heads_Click(object sender, RoutedEventArgs e)
        {
            this.Heads_ClickAsync(sender, e);
        }

        private async Task Heads_ClickAsync(object sender, RoutedEventArgs e)
        {
            if (!actualState.Equals("HEAD"))
            {
                chooseOption.Visibility = Visibility.Collapsed;
                fullTail.Visibility = Visibility.Visible;
                await Task.Delay(100);
                fullTail.Visibility = Visibility.Collapsed;
                midTail.Visibility = Visibility.Visible;
                await Task.Delay(100);
                midTail.Visibility = Visibility.Collapsed;
                midCoin.Visibility = Visibility.Visible;
                await Task.Delay(100);
                midCoin.Visibility = Visibility.Collapsed;
                midHead.Visibility = Visibility.Visible;
                await Task.Delay(100);
                midHead.Visibility = Visibility.Collapsed;
                fullHead.Visibility = Visibility.Visible;
                actualState = "HEAD";
            }
        }

        private async Task Tails_ClickAsync(object sender, RoutedEventArgs e)
        {
            if (!actualState.Equals("TAIL"))
            {
                chooseOption.Visibility = Visibility.Collapsed;
                fullHead.Visibility = Visibility.Visible;
                await Task.Delay(100);
                fullHead.Visibility = Visibility.Collapsed;
                midHead.Visibility = Visibility.Visible;
                await Task.Delay(100);
                midHead.Visibility = Visibility.Collapsed;
                midCoin.Visibility = Visibility.Visible;
                await Task.Delay(100);
                midCoin.Visibility = Visibility.Collapsed;
                midTail.Visibility = Visibility.Visible;
                await Task.Delay(100);
                midTail.Visibility = Visibility.Collapsed;
                fullTail.Visibility = Visibility.Visible;
                actualState = "TAIL";
            }
        }

        private void Tails_Click(object sender, RoutedEventArgs e)
        {
            this.Tails_ClickAsync(sender, e);
        }

        private void Sort_Click(object sender, RoutedEventArgs e)
        {
            chooseOption.Visibility = Visibility.Collapsed;
            Random rnd = new Random();
            int headOrtail = rnd.Next(1, 3);
            if (headOrtail == 1)
            {
                Tails_ClickAsync(null, null);
                tailsCountSort++;
                tryCountSort++;
            }
            else
            {
                Heads_ClickAsync(null, null);
                headsCountSort++;
                tryCountSort++;
            }
            tryCount.Content = "Try count: " + tryCountSort;
            headsCount.Content = "Heads count: " + headsCountSort;
            tailsCount.Content = "Tails count: " + tailsCountSort;

        }
    }
}
