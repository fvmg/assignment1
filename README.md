# Assignment1

Fernando Victor Marques Goncalves

## How to build the project

Initialize the project on Visual Studio and press the start button.

## How to run the project

Choose an option (Heads, tails or Sort). If you choose heads, the heads will be exhibited. 
If you choose tails, the tails will be exhibited. If you click sort, the program will random the coin.

## License.md

I used the license of Apache because is complete and I like their products.

## Test Change